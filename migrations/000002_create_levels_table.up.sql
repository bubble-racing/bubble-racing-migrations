CREATE TABLE IF NOT EXISTS levels (
    user_id VARCHAR (64) NOT NULL,
    title VARCHAR (50) NOT NULL,
    published BOOLEAN NOT NULL,
    plays INTEGER NOT NULL,
    keeps INTEGER NOT NULL,
    map JSONB NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL,
    CONSTRAINT level_pk PRIMARY KEY (user_id, title),
    CONSTRAINT level_ut_uq UNIQUE (user_id, title)
);
