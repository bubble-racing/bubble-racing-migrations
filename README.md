# Bubble Racing Migrations

### Local Linting
```bash
docker run \
  --entrypoint '' \
  --rm \
  --interactive \
  --volume $(pwd)/migrations:/data \
  wesleydeanflexion/sqlfluff \
  find . \
    -iname "*.sql" \
    -exec sqlfluff lint {} \; \
  | sed -Ee "s|/data/\.|$(pwd)|g"
```

### Local Testing
```
docker run -v $PWD/migrations:/sql sqlfluff/sqlfluff:1.0.0 lint --dialect postgres
```