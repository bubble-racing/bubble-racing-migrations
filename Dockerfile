FROM alpine:latest

WORKDIR /root

COPY scripts /root/scripts
COPY migrations /root/migrations

RUN apk add --no-cache \
    bash \
    curl \
    postgresql-client

RUN curl https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz \
      --location \
      -o /root/migrate.tar.gz && \
    tar -xf migrate.tar.gz && \
    ln -s /root/migrate /usr/local/bin/migrate

CMD [ "bash", "/root/scripts/run-migrations.sh" ]