# exit when any command fails
set -e

# Expected inputs
echo "HOST: ${HOST}"
echo "PORT: ${PORT}"
echo "ADMIN_ROLE: ${ADMIN_ROLE}"
echo "ADMIN_PASSWORD: [redacted]"
echo "API_DATABASE: ${API_DATABASE}"
echo "API_ROLE: ${API_ROLE}"
echo "API_PASSWORD: [redacted]"

# Setup postgres authentication
echo "${HOST}:${PORT}:*:${ADMIN_ROLE}:${ADMIN_PASSWORD}" >> ~/.pgpass
chmod 600 ~/.pgpass

# Save some typing
PSQL_CMD="psql --host=${HOST} --port=${PORT} --username=${ADMIN_ROLE} --dbname=postgres --command"

# Create database if it doesn't exist
$PSQL_CMD "CREATE DATABASE ${API_DATABASE}" || echo 'api database already exists'

# Run migrations
DATABASE_URL="postgres://${ADMIN_ROLE}@${HOST}:${PORT}/${API_DATABASE}?sslmode=disable"
migrate --version
migrate \
  -database $DATABASE_URL \
  -path /root/migrations \
  up

# Assign roles to api user
PSQL_CMD="psql --host=${HOST} --port=${PORT} --username=${ADMIN_ROLE} --dbname=${API_DATABASE} --command"
$PSQL_CMD "CREATE ROLE ${API_ROLE}" || echo 'role already exists'
$PSQL_CMD "
  ALTER ROLE ${API_ROLE} WITH LOGIN PASSWORD '${API_PASSWORD}';
  GRANT SELECT, INSERT, UPDATE, DELETE ON gameservers TO ${API_ROLE};
  GRANT SELECT, INSERT, UPDATE, DELETE ON levels TO ${API_ROLE};
"